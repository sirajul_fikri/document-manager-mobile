import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'global.dart' as global;

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = new GlobalKey();

  bool _validate = false;
  String email;
  String password;

  _login() async {
    final response = await http.post("http://10.0.2.2:8000/api/login",
        body: {'email': email, 'password': password});
    var userdata = json.decode(response.body);
    if (userdata['error'] == 'true') {
      showInSnackBar(userdata['message']);
    } else {
      global.id = userdata['id'].toString();
      global.email = userdata['email'];
      global.name = userdata['name'];

      Navigator.pushReplacementNamed(context, '/document');
    }
  }

  _sendToServer() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _login();
    } else {
      setState(() {
        _validate = true;
      });
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          Form(
            key: _formKey,
            autovalidate: _validate,
            child: Center(
              child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.only(left: 24.0, right: 24.0),
                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Email',
                      hintText: 'Enter your email',
                    ),
                    onSaved: (String val) {
                      email = val;
                    },
                  ),
                  TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: 'Password',
                      hintText: 'Enter your password',
                    ),
                    onSaved: (String val) {
                      password = val;
                    },
                  ),
                  RaisedButton(
                    child: Text("Login"),
                    onPressed: () {
                      _sendToServer();
                    },
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
