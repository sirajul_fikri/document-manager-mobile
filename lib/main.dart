import 'package:dmmobile/document.dart';
import 'package:dmmobile/login.dart';
import 'package:dmmobile/note.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(primarySwatch: Colors.blue),
      home: new Login(),
      routes: <String, WidgetBuilder>{
        '/login': (BuildContext context) => new Login(),
        '/document': (BuildContext context) => new Document(),
        '/note': (BuildContext context) => new Note(),
      },
    );
  }
}
