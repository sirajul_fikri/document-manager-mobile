import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Document extends StatefulWidget {
  @override
  _DocumentState createState() => _DocumentState();
}

class _DocumentState extends State<Document> {
  Future<List<Docs>> _getDocs() async {
    var data = await http.get("http://10.0.2.2:8000/api/document/");
    var jsonData = json.decode(data.body);
    List<Docs> docs = [];

    for (var u in jsonData) {
      Docs docsdata = Docs(u["title"], u["document_url"]);
      docs.add(docsdata);
    }

    return docs;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Document")),
      body: new Container(
        child: FutureBuilder(
          future: _getDocs(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding:
                        EdgeInsets.only(left: 5.0, right: 5.0, bottom: 10.0),
                    child: Container(
                      height: 120.0,
                      width: double.infinity,
                      color: Colors.white,
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 20.0,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width - 35.0,
                              child: Text(
                                snapshot.data[index].title,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontFamily: 'Quicksand',
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Row(
                              children: <Widget>[
                                SizedBox(
                                  width: 10.0,
                                ),
                                RaisedButton(
                                  child: Text("Dowload"),
                                  onPressed: () {},
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            }
          },
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Document Manager'),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: Text('Document'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Note'),
              onTap: () {
                Navigator.pushReplacementNamed(context, '/note');
              },
            ),
            ListTile(
              title: Text('Logout'),
              onTap: () {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/login', ModalRoute.withName('/login'));
              },
            ),
          ],
        ),
      ),
    );
  }
}

class Docs {
  final String title;
  final String url;

  Docs(this.title, this.url);
}
