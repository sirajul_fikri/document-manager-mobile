import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Note extends StatefulWidget {
  @override
  _NoteState createState() => _NoteState();
}

class _NoteState extends State<Note> {
  Future<List<Notes>> _getNotes() async {
    var data = await http.get("http://10.0.2.2:8000/api/note/");
    var jsonData = json.decode(data.body);
    List<Notes> notes = [];

    for (var u in jsonData) {
      Notes docsdata = Notes(u["title"], u["note"]);
      notes.add(docsdata);
    }

    return notes;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Note")),
      body: new Container(
        child: FutureBuilder(
          future: _getNotes(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding:
                        EdgeInsets.only(left: 5.0, right: 5.0, bottom: 10.0),
                    child: Container(
                      height: 120.0,
                      width: double.infinity,
                      color: Colors.white,
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 20.0,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width - 35.0,
                              child: Text(
                                snapshot.data[index].title,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontFamily: 'Quicksand',
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width - 35.0,
                              child: Text(
                                snapshot.data[index].note,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontFamily: 'Quicksand',
                                  fontSize: 15.0,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            }
          },
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Document Manager'),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: Text('Document'),
              onTap: () {
                Navigator.pushReplacementNamed(context, '/document');
              },
            ),
            ListTile(
              title: Text('Note'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Logout'),
              onTap: () {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/login', ModalRoute.withName('/login'));
              },
            ),
          ],
        ),
      ),
    );
  }
}

class Notes {
  final String title;
  final String note;

  Notes(this.title, this.note);
}
